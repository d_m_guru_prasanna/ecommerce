<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\Size;
use App\Users;
use App\ProductDetails;
use App\SliderImage;
use View;

class BaseController extends ConstantsController
{
    //
    public function __construct(Categories $categories,Size $size,Users $users,ProductDetails $productdetails,SliderImage $sliderimage){
        $this->categories =$categories;
        $this->productdetails =$productdetails;
        $this->size =$size;
        $this->sliderimage =$sliderimage;
        $this->users =$users;



    }

     public function base_image_upload_with_key($request,$key)
    
    {
        // var_dump($request->$key);exit; keepers_logo.png
        $image = $request->$key;
        //$imageName = $request->file($key)->getClientOriginalName();
        $ext = $image->getClientOriginalExtension();
        $imageName = self::generate_random_string().'.'.$ext;
        $request->file($key)->move(
                'public/uploads/', $imageName
        );
        return $image_url = BASE_URL."public/uploads/" . $imageName;
    }


     public
            function send_email($email, $view_name, $data) {

        Mail::send($view_name, $data, function ($message)use($email) {
            $message->from('karthik@gmail.com', 'Mail from '.APP_NAME);
            $message->to($email);
            $message->subject('Password reset mail from '.APP_NAME);
        });
    }

    public
            function encrypt_password($password) {

        $key = hash('sha256', 'sparkout');
        $iv = substr(hash('sha256', 'developer'), 0, 16);
        $output = openssl_encrypt($password, "AES-256-CBC", $key, 0, $iv);
        $output2 = base64_encode($output);
        return $output2;
    }
    public
            function get_base_url() {
        return BASE_URL;
    }
}
