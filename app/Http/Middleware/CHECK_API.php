<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Log;
use Closure;
use DB;

class CHECK_API
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $auth_id = $request->header('authId');
        $auth_token = $request->header('authToken');
        $check = self::auth_check($auth_id, $auth_token);
        if ($check != 1) {
            return response()->json($check);
        }else{
            return $next($request);
        }
        
    }

    public function auth_check( $auth_id, $auth_token) {
        
        $errors = array();
        $errors['status'] = 'FAILED';

        
        if (!$auth_id) {
            $errors['error_message'] = 'Auth id should not be null';
            return $errors;
        }
        if (!$auth_token) {
            $errors['error_message'] = 'Auth token should not be null';
            return $errors;
        }
        $auth_token = (int) $auth_token;
       
        $check_auth = DB::table('users')
                        ->where('id', $auth_id)
                        ->where('auth_token', $auth_token)->first();
        if(isset($check_auth->id)){

        }else{
            // $errors['error_message'] = 'Auth id , Auth token doesn`t match';
            // return $errors;
        }
        return 1;
    }
}
