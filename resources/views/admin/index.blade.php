@extends('layout.adminmaster')

@section('title')

Ecommerce | Dashboard

@endsection

@section('content')

  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>


      <div class="row">
          <div class="col-xl-4 col-lg-6 col-12">
            <div class="card pull-up">
              <div class="card-content">
                <div class="card-body">
                  <div class="media d-flex">
                    <div class="media-body text-left">
                      <h3 class="info">{{$data}}</h3>
                      <h6>Total Users</h6>
                    </div>
                    <div>
                      <i class="icon-user info font-large-2 float-right"></i>
                    </div>
                  </div>
                  <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                    <div class="progress-bar bg-gradient-x-info" role="progressbar" style="width: 80%"
                    aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-lg-6 col-12">
            <div class="card pull-up">
              <div class="card-content">
                <div class="card-body">
                  <div class="media d-flex">
                    <div class="media-body text-left">
                      <h3 class="warning"> </h3>
                      <h6>Total Supply Providers</h6>
                    </div>
                    <div>
                      <i class="icon-pie-chart warning font-large-2 float-right"></i>
                    </div>
                  </div>
                  <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                    <div class="progress-bar bg-gradient-x-warning" role="progressbar" style="width: 65%"
                    aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-lg-6 col-12">
            <div class="card pull-up">
              <div class="card-content">
                <div class="card-body">
                  <div class="media d-flex">
                    <div class="media-body text-left">
                      <h3 class="success"> </h3>
                      <h6>Total Projects</h6>
                    </div>
                    <div>
                      <i class="icon-user-follow success font-large-2 float-right"></i>
                    </div>
                  </div>
                  <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                    <div class="progress-bar bg-gradient-x-success" role="progressbar" style="width: 75%"
                    aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
           <div class="col-xl-4 col-lg-6 col-12">
            <div class="card pull-up">
              <div class="card-content">
                <div class="card-body">
                  <div class="media d-flex">
                    <div class="media-body text-left">
                      <h3 class="success">146</h3>
                      <h6>Completed Projects</h6>
                    </div>
                    <div>
                      <i class="icon-user-follow success font-large-2 float-right"></i>
                    </div>
                  </div>
                  <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                    <div class="progress-bar bg-gradient-x-success" role="progressbar" style="width: 75%"
                    aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
           <div class="col-xl-4 col-lg-6 col-12">
            <div class="card pull-up">
              <div class="card-content">
                <div class="card-body">
                  <div class="media d-flex">
                    <div class="media-body text-left">
                      <h3 class="success">146</h3>
                      <h6>Pending Projects</h6>
                    </div>
                    <div>
                      <i class="icon-user-follow success font-large-2 float-right"></i>
                    </div>
                  </div>
                  <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                    <div class="progress-bar bg-gradient-x-success" role="progressbar" style="width: 75%"
                    aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
           <div class="col-xl-4 col-lg-6 col-12">
            <div class="card pull-up">
              <div class="card-content">
                <div class="card-body">
                  <div class="media d-flex">
                    <div class="media-body text-left">
                      <h3 class="success">$146</h3>
                      <h6>Total Earnings</h6>
                    </div>
                    <div>
                      <i class="icon-user-follow success font-large-2 float-right"></i>
                    </div>
                  </div>
                  <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                    <div class="progress-bar bg-gradient-x-success" role="progressbar" style="width: 75%"
                    aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--/ eCommerce statistic -->

        <!--/Recent Orders & Monthly Sales -->
                <div class="row">
<div class="col-xl-6 col-lg-12 col-sm-12">
                  <div class="card">
              <div class="card-header">
                <h4 class="card-title">Reports</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right"
                      href="report-summary.html" target="_blank">View all</a></li>
                  </ul>
                </div>
              </div>
              <div class="card-content mt-1">
                <div class="table-responsive">
                  <table id="recent-orders" class="table table-hover table-xl mb-0">
                    <thead>
                      <tr>
                        <th class="border-top-0">Job ID</th>
                        <th class="border-top-0">User Name</th>
                        <th class="border-top-0">Client Name</th>
                        <th class="border-top-0">Service Provider Id </th>
                        <th class="border-top-0">Amount</th>
                       
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="text-truncate">1</td>
                        <td class="text-truncateRecent Sales">
                         MuniRaja
                        </td>
                        <td>
                          Vishnu
                        </td>
                        <td>
                          Premimum
                        </td>
                        <td class="text-truncate">$ 1200.00</td>
                      </tr>
                      <tr>
                        <td class="text-truncate">2</td>
                        <td class="text-truncate ">
                         Raja
                        </td>
                        <td>
                         Krishna
                        </td>
                        <td>
                          Lake View
                        </td>
                        <td class="text-truncate">$ 1190.00</td>
                      </tr>
                      <tr>
                        <td class="text-truncate">3</td>
                        <td class="text-truncate ">
                          Karthick
                        </td>
                        <td>
                         Senthil
                        </td>
                        <td>
                          Ultra premimum
                        </td>
                        <td class="text-truncate">$ 999.00</td>
                      </tr>
                      <tr>
                        <td class="text-truncate">4</td>
                        <td class="text-truncate ">
                         MoniChandru
                        </td>
                        <td>
                          Lokesh
                        </td>
                        <td>
                           Lake View
                        </td>
                        <td class="text-truncate">$ 1150.00</td>
                      </tr>
                      <tr>
                        <td class="text-truncate">5</td>
                        <td class="text-truncate ">
                          Vishnu
                        </td>
                        <td>
                         Aravind
                        </td>
                        <td>
                          Ultra premimum
                        </td>
                        <td class="text-truncate">$ 1180.00</td>
                      </tr>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

                <div class="col-xl-6 col-lg-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Calender</h4>
                </div>
                <div class="card-body">
                  <p class="card-text"></p>
                  <div id="clndr-default" class="overflow-hidden bg-grey bg-lighten-3"></div>
                </div>
              </div>
            </div>
          </div>

      </div>
    </div>
    <div id="clndr" class="clearfix">
            <script type="text/template" id="clndr-template">
              <div class="clndr-controls">
                <div class="clndr-previous-button">&lt;</div>
                <div class="clndr-next-button">&gt;</div>
                <div class="current-month">
                  <%= month %>
                    <%= year %>
                </div>
              </div>
              <div class="clndr-grid">
                <div class="days-of-the-week clearfix">
                  <% _.each(daysOfTheWeek, function(day) { %>
                    <div class="header-day">
                      <%= day %>
                    </div>
                    <% }); %>
                </div>
                <div class="days">
                  <% _.each(days, function(day) { %>
                    <div class="<%= day.classes %>" id="<%= day.id %>">
                      <span class="day-number">
                        <%= day.day %>
                      </span>
                    </div>
                    <% }); %>
                </div>
              </div>
              <div class="event-listing">
                <div class="event-listing-title">Event this month</div>
                <% _.each(eventsThisMonth, function(event) { %>
                  <div class="event-item font-small-3">
                    <div class="event-item-day font-small-2">
                      <%= event.date %>
                    </div>
                    <div class="event-item-name text-bold-600">
                      <%= event.title %>
                    </div>
                    <div class="event-item-location">
                      <%= event.location %>
                    </div>
                  </div>
                  <% }); %>
              </div>
            </script>
          </div>
  </div>
  <!-- ////////////////////////////////////////////////////////////////////////////-->

  <!-- BEGIN VENDOR JS-->
  <script src="{{URL::asset('public/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <script src="{{URL::asset('public/app-assets/vendors/js/charts/chartist.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/charts/chartist-plugin-tooltip.min.js')}}"
  type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/charts/raphael-min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/charts/morris.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/timeline/horizontal-timeline.js')}}" type="text/javascript"></script>
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN MODERN JS-->
  <script src="{{URL::asset('public/app-assets/js/core/app-menu.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/core/app.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/customizer.js')}}" type="text/javascript"></script>
  <!-- END MODERN JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <script src="{{URL::asset('public/app-assets/js/scripts/pages/dashboard-ecommerce.js')}}" type="text/javascript"></script>
    <!-- BEGIN PAGE VENDOR JS-->
  <script src="{{URL::asset('public/app-assets/vendors/js/extensions/moment.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/extensions/underscore-min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/extensions/clndr.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('public/app-assets/js/scripts/extensions/clndr.js')}}" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <script src="{{URL::asset('public/app-assets/vendors/js/charts/chart.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/charts/raphael-min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/charts/morris.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js')}}"
  type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/charts/jvector/jquery-jvectormap-world-mill.js')}}"
  type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/data/jvector/visitor-data.js')}}" type="text/javascript"></script>
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <script src="{{URL::asset('public/app-assets/js/scripts/pages/dashboard-sales.js')}}" type="text/javascript"></script>
  <!-- END PAGE LEVEL JS-->
</body>
</html>
@endsection