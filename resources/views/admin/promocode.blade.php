
@extends('layout.adminmaster')

@section('title')

COM - Ultimate Freelance Marketplace
@endsection

@section('content')       
 <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
          <h3 class="content-header-title mb-0 d-inline-block">Promo Code</h3>
          <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item active">Promo Code Management
                </li>
              </ol>
            </div>
          </div>
        </div>
        
      </div>
      <div class="content-body">
        <section id="icon-tabs">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">PROMO CODE MANAGEMNET</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>

                <div class="card-content collapse show">
                  <div class="card-body">
                    <form action="#" class="icons-tab-steps wizard-notification">

                    <fieldset>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="eventName2">Coupon Code :</label>
                              <input type="text" class="form-control" id="name" value="WaterFall Food Restaurant">
                            </div>
                          </div>
                          <div class="col-md-6"> 
                             <div class="form-group">
                             <label>Start Date Time: </label>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                 <span class="input-group-text">
                                   <span class="la la-calendar-o"></span>
                                </span>
                                </div>
                              <input type='text' class="form-control pickadate-arrow" placeholder="Change Formats"/>
                            </div>
                           </div>
                           </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label for="hours_opening"> </label>

                                    <div class="input-group clockpicker">
                                        <input type="text" class="form-control" name="hours_opening[ALL]" value="00:00" required="">
                                       
                                    </div>
                                </div>
                            </div>
                             <div class="col-md-6"> 
                             <div class="form-group">
                             <label>End Date Time: </label>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                 <span class="input-group-text">
                                   <span class="la la-calendar-o"></span>
                                </span>
                                </div>
                              <input type='text' class="form-control pickadate-arrow" placeholder="Change Formats"/>
                            </div>
                           </div>
                           </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label for="hours_opening"> </label>

                                    <div class="input-group clockpicker">
                                        <input type="text" class="form-control" name="hours_opening[ALL]" value="00:00" required="">
                                       
                                    </div>
                                </div>
                            </div>
                           <div class="col-md-12">
                             <div class="form-group">
                              <label for="content">Short Description:</label>
                             <input type="text" class="form-control" id="details" placeholder="A Short Description">
                             
                            </div>
                          </div>
                           <div class="col-md-12">
                             <div class="form-group">
                              <label for="content">Long Description:</label>
                             <input type="text" class="form-control" id="details" placeholder="A Short Description">
                             
                            </div>
                             <div class="col-md-12">
                           <div class="form-group">
                            <label for="eventLocation2">Type : </label>
                            <label class="radio-inline">
                                <input type="radio" value="non-veg" name="food_type">Flat Off
                            </label>
                            <label class="radio-inline">
                                <input type="radio" value="veg" name="food_type" checked="">Percent Off
                            </label>

                            </div>
                          </div>
                             <div class="col-md-12">
                             <div class="form-group">
                              <label for="pass">Amount To Be Discounted :</label>
                             <input type="text" class="form-control" id="amount" value=" " placeholder="$">
                             </div>
                            </div>
                            <div class="col-md-12">
                             <div class="form-group">
                              <label for="pass">Maximum times a promotion can be used(in total) :</label>
                             <input type="text" class="form-control" id="amount" value=" ">
                             </div>
                            </div>
                             <div class="col-md-12">
                             <div class="form-group">
                              <label for="pass">Maximum usage per customer :</label>
                             <input type="text" class="form-control" id="amount" value=" ">
                             </div>
                            </div>                       
                        </div>

                          <div class="col-md-6">                     
                            <div class="form-actions">
                              <button type="button" class="btn btn-warning mr-1" style="padding: 10px 15px;">
                               <i class="ft-x"></i> Cancel
                                </button>
                              <button type="submit" class="btn btn-primary mr-1" style="padding: 10px 15px;">
                               <i class="ft-check-square"></i> Submit
                                </button>
                            </div>
                          </div>
                        </div>
                      </fieldset>
                    </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
              </section>
            </div>
          </div>
        </div>
  <!-- 
  <footer class="footer footer-static footer-light navbar-border navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2018 <a class="text-bold-800 grey darken-2" href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent"
        target="_blank">PIXINVENT </a>, All rights reserved. </span>
      <span class="float-md-right d-block d-md-inline-blockd-none d-lg-block">Hand-crafted & Made with <i class="ft-heart pink"></i></span>
    </p>
  </footer>-->
  <!-- BEGIN VENDOR JS-->
   <script>
     function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
            function readURL1(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah1')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
          
</script>
    <!-- BEGIN VENDOR JS-->
  <script src="app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://foodie.deliveryventure.com/assets/admin/plugins/clockpicker/dist/bootstrap-clockpicker.min.js"></script>
    <script type="text/javascript">
    function disableEnterKey(e)
    {
        var key;
        if(window.e)
            key = window.e.keyCode; // IE
        else
            key = e.which; // Firefox

        if(key == 13)
            return e.preventDefault();
    }
    $('.clockpicker').clockpicker({
        donetext: "Done"
    });
    $('.dropify').dropify();
    $('#everyday').change(function() {
        if($(this).is(":checked")) {
            $('.everyday').show();
            $('.singleday').hide();
            $('.singleday .chk').prop('checked',false);
            $('.everyday .chk').prop('checked',true);
        }else{
            $('.everyday').hide();
            $('.singleday').show();
            $('.everyday .chk').prop('checked',false);
            $('.singleday .chk').prop('checked',true);
        }
    });
</script>

  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <script src="{{URL::asset('public/app-assets/vendors/js/pickers/pickadate/picker.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/pickers/pickadate/picker.date.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/pickers/pickadate/picker.time.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/pickers/pickadate/legacy.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js')}}"
  type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/pickers/daterange/daterangepicker.js')}}"
  type="text/javascript"></script>
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN MODERN JS-->
  <script src="{{URL::asset('public/app-assets/js/core/app-menu.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/core/app.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/customizer.js')}}" type="text/javascript"></script>
  <!-- END MODERN JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <script src="{{URL::asset('public/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js')}}"
  type="text/javascript"></script>
  <!-- END PAGE LEVEL JS-->
</body>
</html>
@endsection