
@extends('layout.adminmaster')

@section('title')

COM - Ultimate Freelance Marketplace
@endsection

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
          <h3 class="content-header-title mb-0 d-inline-block">Send Email</h3>
          <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item active">Send Email
                </li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="horizontal-form-layouts">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                <h4 class="card-title" id="horz-layout-basic"></i> Send Email Form</h4>

                  <!-- <h4 class="card-title" id="horz-layout-basic">Project Info</h4> -->
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collpase show">
                  <div class="card-body">
                    <div class="card-text">
                      <!-- <p>This is the basic horizontal form with labels on left and form
                        controls on right in one line. Add <code>.form-horizontal</code>                        class to the form tag to have horizontal form styling. To
                        define form sections use <code>form-section</code> class
                        with any heading tags.</p> -->
                    </div>

                    <form class="form form-horizontal">
                      <div class="form-body">
                        <div class="form-group row">
                          <label class="col-md-3 label-control" for="projectinput1">E-mail to</label>
                          <div class="col-md-9">

                      <div class="show" style="display: block; position: static; width: 100%; margin-top: 0;">
                        <div class="">
                          <div class="input-group">
                  <div>
                    <fieldset>
                      <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" name="tab" id="customRadio1" value="igotnone" onclick="show1();">
                        <label class="custom-control-label" for="customRadio1">All</label>
                      </div>
                    </fieldset>
                  </div> &nbsp;&nbsp;&nbsp;
                  <div>
                    <fieldset>
                      <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" name="tab" id="customRadio2" value="igottwo" onclick="show2();">
                        <label class="custom-control-label" for="customRadio2">Specified</label>
                      </div>
                    </fieldset>
                  </div>
                            </div>
                            
                        </div>
                      </div>
                    </div>
                          </div>
                        
                        <div  id="div1" class="hide" style="display: none;">
                        <div class="form-group row">
                          <label class="col-md-3 label-control" for="projectinput6">Email address</label>
                          <div class="col-md-9">
                            <select id="projectinput6" name="interested" class="form-control">
                              <option value="1" selected="" disabled="">email 1</option>
                              <option value="2">email 2</option>
                              <option value="3">email 3</option>
                            </select>
                          </div>
                        </div>
                      </div>
                        
                        <div class="form-group row">
                          <label class="col-md-3 label-control" for="projectinput4">Subject</label>
                          <div class="col-md-9">
                            <input type="text" id="projectinput4" class="form-control" placeholder="Subject" name="subject">
                          </div>
                        </div>

                        <div class="form-group row">
                          <label class="col-md-3 label-control" for="projectinput4">Message (Salutation will be automatically added)</label>
                          <div class="col-md-9">
                            <div id="editor">
                              <div id='edit'>
                              </div>
                        </div> 
                          </div>
                        </div>
                        </div>
                      <div class="form-actions center">
                        <button type="button" class="btn btn-warning mr-5" style="padding: 10px 15px;">
                          <i class="ft-x"></i> Cancel
                        </button>
                        <button type="submit" class="btn btn-primary" style="padding: 8px 15px;">
                          <i class="la la-check-square-o"></i> Submit
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- // Basic form layout section end -->
      </div>
    </div>
  </div>

  <script>
function show1(){
  document.getElementById('div1').style.display ='none';
}
function show2(){
  document.getElementById('div1').style.display = 'block';
}
</script>
  <!-- ////////////////////////////////////////////////////////////////////////////-->
  <footer class="footer footer-static footer-light navbar-border navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2018 <a class="text-bold-800 grey darken-2" href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent"
        target="_blank">PIXINVENT </a>, All rights reserved. </span>
      <span class="float-md-right d-block d-md-inline-blockd-none d-lg-block">Hand-crafted & Made with <i class="ft-heart pink"></i></span>
    </p>
  </footer>
  <!-- BEGIN VENDOR JS-->
  <script src="{{URL::asset('public/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
    <!-- BEGIN PAGE VENDOR JS-->
  <script src="{{URL::asset('public/app-assets/vendors/js/forms/icheck/icheck.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/forms/toggle/switchery.min.js')}}" type="text/javascript"></script>
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN MODERN JS-->
  <script src="{{URL::asset('public/app-assets/js/core/app-menu.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/core/app.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/customizer.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/dropdowns/dropdowns.js')}}" type="text/javascript"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>

  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/froala_editor.min.js')}}" ></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/align.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/char_counter.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/code_beautifier.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/code_view.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/colors.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/draggable.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/emoticons.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/entities.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/file.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/font_size.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/font_family.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/fullscreen.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/image.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/image_manager.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/line_breaker.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/inline_style.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/link.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/lists.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/paragraph_format.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/paragraph_style.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/quick_insert.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/quote.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/table.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/save.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/url.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/video.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/help.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/print.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/third_party/spell_checker.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/special_characters.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/app-assets/editor/js/plugins/word_paste.min.js')}}"></script>

  <script>
    $(function(){
      $('#edit').froalaEditor()
    });
  </script>

  <!-- END MODERN JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <!-- END PAGE LEVEL JS-->
</body>
</html>
@endsection