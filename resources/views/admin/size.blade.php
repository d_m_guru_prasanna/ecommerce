@extends('layout.adminmaster')

@section('title')
Ecommerce | size
@endsection

@section('content')
<div class="app-content content"> <script  src="{{URL::asset('public/app-assets/js/core/libraries/jquery.min.js')}}" type="text/javascript" ></script>
  <style type="text/css">
    .p-2{
      padding: 1rem !important;
  }
  </style>

  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Size Jobs</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active">Size List
              </li>
            </ol>
          </div>
        </div>
      </div>
      <div class="content-header-right col-md-6 col-12 pb-1">
        <div class="dropdown float-md-right">
          <a class="btn btn-success round btn-glow px-2" data-toggle="modal"  data-target="#large" href="#"> <i class="la la-plus"></i> Add Size </a>
        </div>
      </div>
    </div>
    <div class="content-body">
      <!-- Basic form layout section start -->

      <section id="configuration">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-head">
                <div class="card-header">

                  <h4 class="card-title">SIZE JOBS</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                   <ul class="list-inline mb-0">
                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="row">
                <div class="col-lg-8 push-lg-2">
                    
            @if(Session::has('error'))
                <p class="alert alert-danger" style="margin-top: 20px;">{{ Session::get('error') }}</p>
            @endif

            @if(Session::has('success'))
                <p class="alert alert-success" style="margin-top: 20px;">{{ Session::get('success') }}</p>
            @endif
                </div>  
            </div>
            <div class="card-content collapse show">
              <div class="card-body card-dashboard">
                <div class="table-responsive">
                  <table id="category_list" class="table table-striped table-bordered zero-configuration">
                    <thead> 
                      <tr>
                        <th>Sl.No</th>
                        <th>Size Name</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($data as $d)
                      <tr>
                        <td>{{$d->id}}</td>
                        <td>{{$d->size}}</td>
                        <td><button class='btn btn-success edit' data-toggle='modal'  data-target='#large{{$d->id}}'><i class='fa fa-edit'></i></button>
                          <a href="{{URL('/')}}/delete_size/{{$d->id}}" class="btn btn-danger"><i class='fa fa-trash'></i></a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="modal-sizes">
      <!-- Modal -->
      <div class="modal fade text-left" id="large" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17"
      aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel17">Add Size</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <section id="demo">
              <div class="row">
                <div class="col-12">
                  <div class="">
                    <div class="card-content collapse show">
                      <div class="card-body">
                        <form action="{{URL('/')}}/addedit_size" method="post">
                          <!-- Step 1 -->
                          <fieldset>
                            <input type="hidden" name="id" id="c_id">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                  <label for="firstName1">Size Name :</label>
                                  <input type="text" class="form-control" id="sub_category" name="size_name">
                                </div>
                              </div>
                              <div class="col-md-12">
                                <button class="btn btn-info p-2" style="float: right;">Submit</button>
                              </div>
                            </div>
                          </fieldset>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- // Basic form layout section end -->

  <!--- edit category---->
@foreach($data as $d1)
    <section id="modal-sizes">
      <!-- Modal -->
      <div class="modal fade text-left" id="large{{$d1->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17"
      aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel17">Edit Size</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <section id="demo">
              <div class="row">
                <div class="col-12">
                  <div class="">
                    <div class="card-content collapse show">
                      <div class="card-body">
                        <form action="{{URL('/')}}/addedit_size" method="post">
                          <!-- Step 1 -->
                          <fieldset>
                            <input type="hidden" name="id" id="c_id" value="{{$d1->id}}">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                  <label for="firstName1">Size Name :</label>
                                  <input type="text" class="form-control" id="sub_category" value="{{$d1->size}}" name="size_name">
                                </div>
                              </div>
                              <div class="col-md-12">
                                <button class="btn btn-info p-2" style="float: right;">Submit</button>
                              </div>
                            </div>
                          </fieldset>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  </section>
  @endforeach
</div>
</div>
</div>
  <!-- 
  <footer class="footer footer-static footer-light navbar-border navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2018 <a class="text-bold-800 grey darken-2" href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent"
        target="_blank">PIXINVENT </a>, All rights reserved. </span>
      <span class="float-md-right d-block d-md-inline-blockd-none d-lg-block">Hand-crafted & Made with <i class="ft-heart pink"></i></span>
    </p>
  </footer>-->
  <!-- BEGIN VENDOR JS-->
  <script src="{{URL::asset('public/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <script src="{{URL::asset('public/app-assets/vendors/js/tables/datatable/datatables.min.js')}}" type="text/javascript"></script>

  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN MODERN JS-->
  <script src="{{URL::asset('public/app-assets/js/core/app-menu.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/core/app.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/customizer.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/dropdowns/dropdowns.js')}}" type="text/javascript"></script>

  <!-- END MODERN JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <script src="{{URL::asset('public/app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"
  type="text/javascript"></script>
  <!-- END PAGE LEVEL JS-->
</body>
</html>
@endsection