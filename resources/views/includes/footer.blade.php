 <a id="scrollup">Scroll</a>
<div class="footer">
	  <div class="container">
	   <div class="row">
	  
	    <div class="col-md-4 col-sm-6 text-left">
	     <h4 class="heading no-margin">About Us</h4>
		 <hr class="mint">
		 <p style="color: #373737 !important;">The COM is the Ultimate Freelance Marketplace  for employers and marketers to connect, collaborate, and get work done.</p>
		 <p style="color: #373737 !important;">We work hard to build a great product that is beautifully designed, simple to use, user friendly with great focus on user experience and customer service.</p>
	    </div><!-- /.col-md-4 -->
	   
	    <div class="col-md-2 col-sm-6 text-left">
	     <h4 class="heading no-margin">Company</h4>
		 <hr class="mint">
		 <div class ="no-padding">
		  <a href="#">Home</a>
		  <a href="#">About</a>
		  <a href="{{URL('/')}}/post">Jobs</a>
		  <a href="#">marketers</a>
		  <a href="#">How it works</a>
		  <a href="#">Contact</a>		 
		 </div>
	    </div><!-- /.col-md-2 -->	
		
		<div class="col-md-3 col-sm-6 text-left">
	     <h4 class="heading no-margin">Other Services</h4>
		 <hr class="mint">
		 <div class="no-padding">
		  <a href="#">Privacy Policy</a>
		  <a href="#">Terms of Use</a>
		  <a href="#">FAQ</a>		 
		 </div>
	    </div><!-- /.col-md-3 -->	
		
	    <div class="col-md-3 col-sm-6 text-left">
	    <h4 class="heading no-margin">Browse</h4>
		<hr class="mint">
		 <div class="no-padding">
		   <a href="#">Top marketers this Month of June</a>
		   <a href="#">Top marketers by Skill</a>
		   <a href="#">Top marketers in USA</a>
		   <a href="#">Top marketers in UK</a>
		   <a href="#">Top marketers in Australia</a>
		   <a href="#">Top marketers in India</a>		  
		  </div>
		 </div><!-- /.col-md-3 -->
		 
	    </div><!-- /.row -->
	   <div class="clearfix"></div>
	  </div><!-- /.container-->
     </div><!-- /.footer -->			
	 
	 <!-- ==============================================
	 Made Section
	 =============================================== -->
	 
	 
	 <!-- ==============================================
	 Bottom Footer Section
	 =============================================== -->	
     <footer id="main-footer" class="main-footer">
	  <div class="container">
	   <div class="row">
	   
	    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
		 <ul class="social-links">
		  <li class="revealOnScroll" data-animation="slideInLeft" data-timeout="800"><a href="#link"><i class="fa fa-facebook fa-fw"></i></a></li>
		  <li class="revealOnScroll" data-animation="slideInLeft" data-timeout="600"><a href="#link"><i class="fa fa-twitter fa-fw"></i></a></li>
		  <li class="revealOnScroll" data-animation="slideInLeft" data-timeout="400"><a href="#link"><i class="fa fa-google-plus fa-fw"></i></a></li>
		  <li class="revealOnScroll" data-animation="slideInLeft" data-timeout="200"><a href="#link"><i class="fa fa-pinterest fa-fw"></i></a></li>
		  <li class="revealOnScroll" data-animation="slideInLeft"><a href="#link"><i class="fa fa-linkedin fa-fw"></i></a></li>
		 </ul>
		</div>
	    <!-- /.col-sm-4 -->
		
		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 revealOnScroll" data-animation="bounceIn" data-timeout="200">
		 <div class="img-responsive text-center">
		 <img src="{{URL::asset('/public/asset/img/logo_1.png')}}" style="width:118px !important;height: 98px !important">		 </div><!-- End image-->
		</div>
		<!-- /.col-sm-4 -->
		
		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-right revealOnScroll" data-animation="slideInRight" data-timeout="200">
		 <p style="color: black !important;">&copy;2018 COM Designed by <a href="http://www.sparkouttech.com" target="_blank">Sparkout tech.</a></p>
		</div>
		<!-- /.col-sm-4 -->
				
	   </div><!-- /.row -->
	   <br><br><br>

@if(Session::get( 'is_activated')==0 && Session::get( 'user_name' ) !="")
 <div class="activate navbar-fixed-bottom col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2" >
    <div class="navbar-header"><span class="navbar-brand remove_hover"  >Hi, {{Session::get( 'user_name' )}} Please Activate Your Account</span> </div><a href="{{URL('/')}}/send_activation?id={{Session::get( 'user_id' )}}"><span class="navbar-brand align_btn kafe-btn kafe-btn-mint-small" >
    Send Your Activation Email</span></a>
   
   
</div>
 @endif
	  </div><!-- /.container -->
	 </footer><!-- /.footer -->  
	 

